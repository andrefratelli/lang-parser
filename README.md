# README #

A very simple compiler written for a custom language. The code is compiled to MIPS assembly code. I don't recall exactly which compiler optimisations where used, but they should include the following:

* Register and variable liveness
* Constant expressions
* Some AST optimisations, don't recall which
* Probably others.

lang code sample which uses all implemented language constructs:

```
#!lang

read x;
y = 0;

repeat
        if x % 2 == 0 || x % 4 > 1 then
                y = y + 1;
        else
                x = x - 1 * 1;
        endif;
        x = x - 1;
until !(x < 0 && y != 0);

if y >= 0 || y <= 0 then
        write -y | (+1 ^ 2 & x);
endif;
write x;

```


This rep is not maintained.