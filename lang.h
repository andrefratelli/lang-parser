#ifndef __LANG_H
#define __LANG_H

#include "langc.h"
#include <stdio.h>

struct T_options {
        FILE* _asm;
        FILE* _tree;
        FILE* _in;
        bool_type _stdin;
        bool_type _valid;
};

struct T_options lang_parseOptions(int argc, char** argv);

void lang_out_syntax(string_type arg);

void lang_out_depth(FILE* stream, size_type depth);
void lang_out_tree(FILE* stream, struct T_block* block);
void lang_out_block(FILE* stream, struct T_block* block, size_type depth);
void lang_out_exp(FILE* stream, struct T_exp* exp, size_type depth);
void lang_out_state(FILE* stream, struct T_state* exp, size_type depth);
/*
void lang_out_var(FILE* stream, struct T_variable* var, size_type depth);
void lang_out_const(FILE* stream, struct T_constant* cons, size_type depth);
void lang_out_binary(FILE* stream, struct T_binary* bin, size_type depth);
void lang_out_unary(FILE* stream, struct T_unary* un, size_type depth);
*/
void lang_out_symbols(FILE* stream, struct T_compileSymbols scope);
void lang_out_asm(FILE* stream, struct T_asm* src, struct T_compileSymbols scope);
void lang_out_asm_iter(FILE* stream, struct T_asm* src, struct T_compileSymbols scope);

#endif

