#ifndef __LANGDEFS_H
#define __LANGDEFS_H

typedef char* string_type;
typedef int value_type;
typedef unsigned int size_type;
typedef int bool_type;

struct T_exp;
struct T_state;

struct T_unary;
struct T_binary;
struct T_variable;
struct T_constant;

struct T_block;

struct T_repeat;
struct T_if;
struct T_read;
struct T_write;
struct T_assign;

#include "lang.tab.h"

enum state_type {
        STATE_REPEAT,
        STATE_IF,
        STATE_READ,
        STATE_WRITE,
        STATE_ASSIGN
};

enum exp_type {
        EXP_VARIABLE,
        EXP_CONSTANT,
        EXP_SYMMETRIC,
        //EXP_IDENTITY,
        EXP_MOD,
        EXP_DIV,
        EXP_MULT,
        EXP_SUBTRACT,
        EXP_ADD,
        EXP_BNEG,
        EXP_BAND,
        EXP_BXOR,
        EXP_BOR,
        EXP_LT,
        EXP_LE,
        EXP_GT,
        EXP_GE,
        EXP_AND,
        EXP_OR,
        EXP_NOT,
        EXP_EQ,
        EXP_DIFF
};

enum type_type {
        TYPE_INT,
        TYPE_BOOL
};

struct T_exp {
        union {
                struct T_unary* _unary;
                struct T_binary* _binary;
                struct T_variable* _variable;
                struct T_constant* _constant;
        } _ctx;

        enum exp_type _type;
        enum type_type _vt;
};

struct T_state {
        union {
                struct T_repeat* _repeat;
                struct T_if* _if;
                struct T_read* _read;
                struct T_write* _write;
                struct T_assign* _assign;
        } _ctx;

        enum state_type _type;
};

struct T_variable {
        string_type _name;
        size_type _lineno;
        /*
         * :: HACK ::
         *
         * Variables are only checked at compile time, not parse time, when we
         * already do not possess line info. We need this field to know where
         * the variable has been declared to present as an error in case such
         * is necessary.
         */
};

struct T_constant { value_type _value; };
struct T_unary { struct T_exp* _exp; };
struct T_binary {
        struct T_exp* _left;
        struct T_exp* _right;
};

struct T_block {
        struct T_block* _next;
        struct T_state* _state;
};

struct T_repeat {
        struct T_exp* _exp;
        struct T_block* _block;
};

struct T_if {
        struct T_exp* _exp;
        struct T_block* _block;
        struct T_block* _fail;
};

struct T_read {
        struct T_variable* _exp;
};

struct T_write {
        struct T_exp* _exp;
};

struct T_assign {
        struct T_variable* _lval;
        struct T_exp* _rval;
};

struct T_exp* makeUnaryExp(enum exp_type type, void* value, enum type_type vt);
struct T_exp* makeBinaryExp(enum exp_type type, struct T_exp* left, struct T_exp* right, enum type_type vt);

struct T_state* makeState(enum state_type type, ...);
struct T_block* makeBlock(struct T_state* state, struct T_block* block);

size_type nerr();
bool_type assert_t(bool_type check);
bool_type assert_tm(bool_type check, string_type pmsg);
bool_type assert_var(bool_type check, struct T_variable* pvar);

bool_type isBinary(struct T_exp* exp);
bool_type isConst(struct T_exp* exp);
bool_type isVar(struct T_exp* exp);

#endif

