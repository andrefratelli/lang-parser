#ifndef __HEAP_H
#define __HEAP_H

#define INITIAL_HEAP_SIZE 2
#define HEAP_UNDEF ((size_type)-1)

#include "langdefs.h"

struct heap {
        string_type* _heap;
        size_type _size;
        size_type _cap;
};

void heap_make(struct heap* h);
void heap_destroy(struct heap* h);
size_type heap_find(struct heap* h, string_type string);
bool_type heap_contains(struct heap* h, string_type string);
void heap_push(struct heap* h, string_type string);
void heap_remove(struct heap* h, string_type string);
size_type heap_size(struct heap* h);

#ifndef __REG_TYPE__
#define __REG_TYPE__
typedef unsigned int abstract_reg_type;
typedef unsigned int abstract_label_type;
typedef abstract_reg_type reg_type;
#endif

struct heap_map_entry {
        reg_type _value;
        abstract_reg_type _key;
};

struct heap_map {
        struct heap_map_entry* _heap;
        size_type _size;
        size_type _cap;
};

void heap_map_make(struct heap_map* h);
void heap_map_destroy(struct heap_map* h);
size_type heap_map_find(struct heap_map* h, abstract_reg_type _key);
bool_type heap_map_contains(struct heap_map* h, abstract_reg_type key);
void heap_map_push(struct heap_map* h, struct heap_map_entry entry);
void heap_map_remove(struct heap_map* h, abstract_reg_type _key);
size_type heap_map_size(struct heap_map* h);

#endif

