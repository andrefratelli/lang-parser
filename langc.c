#include "langc.h"
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <math.h>
#include <ctype.h>
#include <stdio.h>

abstract_reg_type* __lang_reserved = NULL;
string_type __lang_parsing = NULL;
size_type __lang_parsing_index = 0;

void __lang_parse_reg(string_type str)
{
        __lang_parsing = str;
        __lang_parsing_index = 0;
}

string_type __lang_parse_next_reg()
{
        if (__lang_parsing != NULL)
        {
                size_type size = strlen(__lang_parsing);
                size_type it, count;
                string_type extraction;

                while (__lang_parsing_index < size)
                {
                        if (__lang_parsing[__lang_parsing_index] == '%')
                        {
                                if (__lang_parsing_index + 1 < size && __lang_parsing[__lang_parsing_index + 1] == 'r')
                                {
                                        it = __lang_parsing_index + 2;
                                        count = 0;

                                        while (it < size && isdigit(__lang_parsing[it])) { it++; count++; }

                                        if (count > 0)
                                        {
                                                extraction = (string_type)malloc(count + 1);

                                                for (it=0 ; it<count ; it++)
                                                {
                                                        extraction[it] = __lang_parsing[__lang_parsing_index + it + 2];
                                                }
                                                extraction[count] = 0;

                                                __lang_parsing_index += count + 2;

                                                return extraction;
                                        }
                                }
                        }
                        __lang_parsing_index++;
                }
        }
        return NULL;
}

string_type lang_map_registers(string_type token, struct hash_map* map)
{
        string_type result = (string_type)malloc(strlen(token));
        string_type aux = (string_type)malloc(strlen(token));
        string_type _reg_str;
        size_type start, jt, it;

        __lang_parse_reg(token);
        result[0] = '\0';

        for (start=__lang_parsing_index ; _reg_str = __lang_parse_next_reg() ; start=__lang_parsing_index)
        {
                jt = strlen(result);

                // Copy everything up to the token
                for (it=start ; token[it] != '%' ; it++) { result[jt++] = token[it]; }

                // Insert $tn
                result[jt] = '$';
                result[jt + 1] = 't';
                result[jt + 2] = '%';
                result[jt + 3] = 'd';
                result[jt + 4] = '\0';
                sprintf(aux, result, hash_map_get(map, atoi(_reg_str)));
                strcpy(result, aux);
        }

        // Copy the rest
        for (it=strlen(result) ; token[start] != '\0' ; start++, it++)
        {
                result[it] = token[start];
        }

        return result;
}

string_type lang_resolve_registers(struct T_token* tok, struct T_compileSymbols* scope, struct hash_map* map, string_type token)
{
        struct heap_map_entry assoc;
        string_type _reg_str;
        abstract_reg_type absreg;
        reg_type reg;
        size_type it;

        if (tok != NULL)
        {
                // Map abstract registers to real registers
                __lang_parse_reg(token);

                while (_reg_str = __lang_parse_next_reg())
                {
                        absreg = atoi(_reg_str);
                        reg = hash_map_get(map, absreg);

                        if (reg == HASH_UNDEF)
                        {
                                for (it=0 ; it<REGISTER_COUNT ; it++)
                                {
                                        if (scope->_registers[it])
                                        {
                                                assoc._key = absreg;
                                                reg = assoc._value = it;

                                                hash_map_insert(map, assoc);

                                                scope->_registers[it] = 0;

                                                break;
                                        }
                                }
                        }
                }
        }

        string_type compiled = lang_map_registers(token, map);

        // Free registers
        for (it=0 ; it<tok->_free._size ; it++)
        {
                reg = hash_map_get(map, tok->_free._registers[it]);

                if (reg != HEAP_UNDEF)
                {
                        scope->_registers[reg] = 1;
                        hash_map_remove(map, tok->_free._registers[it]);
                }
        }

        return compiled;
}

struct T_asm* lang_compile_asm(struct T_token* tok, struct T_compileSymbols* scope, struct hash_map* map)
{
        struct T_asm* _asm = NULL;

        if (tok != NULL)
        {
                _asm = (struct T_asm*)malloc(sizeof(struct T_asm));
                _asm->_next = lang_compile_asm(tok->_next, scope, map);
                _asm->_token = lang_resolve_registers(tok, scope, map, tok->_ctx._text);
                _asm->_label = tok->_label;
        }

        return _asm;
}

struct T_asm* lang_compile(struct T_block* block, struct T_compileSymbols* scope)
{
        struct T_token* tok = NULL;
        struct T_asm* assembly = NULL;
        struct hash_map map;
        size_type it;

        scope->_reg = scope->_label = 0;

        lang_compileBlock(&tok, block, scope);

        if (nerr())
        {
                fprintf(stderr, "[lang] Errors exist. Exiting.\n");
                exit(1);
        }

        if (tok != NULL)
        {
                for (it=0 ; it<REGISTER_COUNT ; it++) { scope->_registers[it] = 1; }

                hash_map_make(&map, 10);
                assembly = lang_compile_asm(tok, scope, &map);
                hash_map_destroy(&map);
        }
        return assembly;
}

void lang_compileBlock(struct T_token** tok, struct T_block* block, struct T_compileSymbols* scope)
{
        while (block != NULL) {
                lang_compileStatement(tok, block->_state, scope);
                block = block->_next;
        }
}

void lang_compileStatement(struct T_token** tok, struct T_state* state, struct T_compileSymbols* scope)
{
        abstract_label_type label, auxl;
        abstract_reg_type reg, auxr;

        size_type it;

        switch (state->_type)
        {
                case STATE_REPEAT:
                        /*
                         * label: <block>
                         *        %r = <evaluate>
                         *        beq %r, $zero, label
                         *        nop
                         */
                        label = lang_makeLabel(tok, scope);                        
                        lang_compileLabel(tok, scope, label);
                        lang_compileBlock(tok, state->_ctx._repeat->_block, scope);

                        reg = lang_makeReg(tok, scope);
                        lang_compileExpression(tok, state->_ctx._repeat->_exp, scope, reg);
                        lang_forceConstLoad(tok, reg);
                        lang_compileToken(tok, "beq %%r%d, $zero, __l%d", reg, label);
                        lang_compileToken(tok, I_NOP);
                        lang_freeReg(tok, scope, reg);
                break;

                case STATE_IF:
                        /*
                         *       %r = <evaluate>
                         *       beq %r, $zero, else
                         *       nop
                         *       <block>
                         *       b endif
                         *       nop
                         * else: <block>
                         * endif:
                         */
                        reg = lang_makeReg(tok, scope);
                        label = lang_makeLabel(tok, scope);        // else
                        auxl = lang_makeLabel(tok, scope);         // endif

                        lang_compileExpression(tok, state->_ctx._if->_exp, scope, reg);
                        lang_forceConstLoad(tok, reg);
                        lang_compileToken(tok, "beq %%r%d, $zero, __l%d", reg, label);
                        lang_freeReg(tok, scope, reg);
                        lang_compileToken(tok, I_NOP);
                        lang_compileBlock(tok, state->_ctx._if->_block, scope);
                        lang_compileToken(tok, "b __l%d", auxl);
                        lang_compileToken(tok, I_NOP);
                        lang_compileLabel(tok, scope, label);
                        lang_compileBlock(tok, state->_ctx._if->_fail, scope);
                        lang_compileLabel(tok, scope, auxl);
                break;

                case STATE_READ:
                        /*
                         * li $v0, 5
                         * syscall
                         * la $reg, var
                         * sw $v0, 0($reg)
                         */
                        reg = lang_makeReg(tok, scope);

                        hash_insert(&scope->_symbols, state->_ctx._read->_exp->_name);
                        lang_compileToken(tok, "li $v0, 5");
                        lang_compileToken(tok, "syscall");
                        lang_compileToken(tok, "la %%r%d, %s", reg, state->_ctx._read->_exp->_name);
                        lang_compileToken(tok, "sw $v0, 0(%%r%d)", reg);
                        lang_freeReg(tok, scope, reg);
                break;
                case STATE_WRITE:
                        /*
                         * move $a0, $src
                         * li $v0, 8
                         * syscall
                         */
                        reg = lang_makeReg(tok, scope);

                        lang_compileExpression(tok, state->_ctx._write->_exp, scope, reg);
                        lang_forceConstLoad(tok, reg);
                        lang_compileToken(tok, "move $a0, %%r%d", reg);
                        lang_compileToken(tok, "li $v0, 1");
                        lang_compileToken(tok, "syscall");

                        lang_compileToken(tok, "li $v0, 4");
                        lang_compileToken(tok, "la $a0, %s", LANG_NEWLINE);
                        lang_compileToken(tok, "syscall");

                        lang_freeReg(tok, scope, reg);
                break;

                case STATE_ASSIGN:
                        /*
                         * %s = <evaluate>
                         * la %d, <var>
                         * sw %s, 0(%d)
                         */
                        reg = lang_makeReg(tok, scope);
                        auxr = lang_makeReg(tok, scope);
                        lang_compileExpression(tok, state->_ctx._assign->_rval, scope, reg);
                        lang_forceConstLoad(tok, reg);
                        hash_insert(&scope->_symbols, state->_ctx._read->_exp->_name);
                        lang_compileToken(tok, "la %%r%d, %s", auxr, state->_ctx._read->_exp->_name);
                        lang_compileToken(tok, "sw %%r%d, 0(%%r%d)", reg, auxr);
                        lang_freeReg(tok, scope, auxr);
                        lang_freeReg(tok, scope, reg);
                break;
        }
}

void lang_compileExpression(struct T_token** tok, struct T_exp* exp, struct T_compileSymbols* scope, abstract_reg_type dest)
{
        abstract_reg_type left;
        abstract_reg_type right;
        struct T_token* tright = NULL;

        if (isVar(exp)) { left = lang_makeReg(tok, scope); }
        else if (!isConst(exp))
        {
                if (isBinary(exp))
                {
                        left = lang_makeReg(tok, scope);
                        lang_compileExpression(tok, exp->_ctx._binary->_left, scope, left);

                        right = lang_makeReg(tok, scope);
                        lang_compileExpression(&tright, exp->_ctx._binary->_right, scope, right);
                }
                else
                {
                        left = lang_makeReg(tok, scope);
                        lang_compileExpression(tok, exp->_ctx._unary->_exp, scope, left);
                }
        }

        switch (exp->_type)
        {
                case EXP_VARIABLE:
                        /*
                         * la $left, __name__
                         * lw $dest, 0($left)
                         */
                        assert_var(hash_contains(&scope->_symbols, exp->_ctx._variable->_name), exp->_ctx._variable);

                        lang_compileToken(tok, "la %%r%d, %s", left, exp->_ctx._variable->_name);
                        lang_compileToken(tok, "lw %%r%d, 0(%%r%d)", dest, left);
                break;

                case EXP_CONSTANT:
                        lang_compileConst(tok, exp->_ctx._constant, scope);
                break;

                case EXP_SYMMETRIC:
                        /*
                         * sub $dest, $zero, $left
                         */
                        if ((*tok)->_const) { (*tok)->_ctx._value = __lang_symmetric((*tok)->_ctx._value); }
                        else { lang_compileToken(tok, "sub %%r%d, $zero, %%r%d", dest, left); }
                break;

                case EXP_BNEG:
                        /*
                         * not $dest, $left
                         */
                        lang_forceConstLoad(tok, left);
                        lang_compileToken(tok, "not %%r%d, %%r%d", dest, left);
                break;

                case EXP_NOT:
                        /*
                         * li $dest, 1
                         * movn $dest, $zero, $left
                         */
                        lang_compileToken(tok, "li %%r%d, 1", dest);
                        lang_forceConstLoad(tok, left);
                        lang_compileToken(tok, "movn %%r%d, $zero, %%r%d", dest, left);
                break;

                case EXP_AND:
                        /*
                         * li $dest, 1
                         * movz $dest, $zero, $left
                         * movz $dest, $zero, $right
                         */
                        lang_compileToken(tok, "li %%r%d, 1", dest);

                        lang_forceConstLoad(tok, left);
                        lang_compileToken(tok, "movz %%r%d, $zero, %%r%d", dest, left);
                        lang_forceConstLoad(tok, right);
                        lang_compileToken(tok, "movz %%r%d, $zero, %%r%d", dest, right);
                break;

                // Binary, arithmetic, withouth an immediate version that read the
                // result from either HI or LO.
                case EXP_MOD: lang_compileBinary(tok, &tright, exp->_ctx._binary, scope, I_DIV, __lang_mod, MFHI, dest, left, right); break;
                case EXP_DIV: lang_compileBinary(tok, &tright, exp->_ctx._binary, scope, I_DIV, __lang_div, MFLO, dest, left, right); break;
                case EXP_MULT: lang_compileBinary(tok, &tright, exp->_ctx._binary, scope, I_MUL, __lang_mult, MFNONE, dest, left, right); break;

                // Binary, arithmetic, without an immediate version.
                case EXP_SUBTRACT: lang_compileBinary(tok, &tright, exp->_ctx._binary, scope, I_SUB, __lang_sub, MFNONE, dest, left, right); break;

                // Binary, arithmetic, with an immediate version.
                // Order may be reversed.
                case EXP_ADD:  lang_compileBinary_i(tok, &tright, exp->_ctx._binary, scope, I_ADD, __lang_add, MFNONE, dest, left, right); break;
                case EXP_BAND: lang_compileBinary_i(tok, &tright, exp->_ctx._binary, scope, I_AND, __lang_and, MFNONE, dest, left, right); break;
                case EXP_BOR:  lang_compileBinary_i(tok, &tright, exp->_ctx._binary, scope, I_OR, __lang_or, MFNONE, dest, left, right); break;
                case EXP_BXOR: lang_compileBinary_i(tok, &tright, exp->_ctx._binary, scope, I_XOR, __lang_xor, MFNONE, dest, left, right); break;

                // Binary, boolean, with an immediate version. The operands are
                // not constants, so the "application" never triggers.
                // Order may be reversed.
                case EXP_OR: lang_compileBinary_i(tok, &tright, exp->_ctx._binary, scope, I_OR, __lang_or, MFNONE, dest, left, right); break;
                case EXP_LT: lang_compileBinary_i(tok, &tright, exp->_ctx._binary, scope, I_SLT, __lang_slt, MFNONE, dest, left, right); break;
                case EXP_LE: lang_compileBinary(tok, &tright, exp->_ctx._binary, scope, I_SLE, __lang_le, MFNONE, dest, left, right); break;
                case EXP_GT: lang_compileBinary(tok, &tright, exp->_ctx._binary, scope, I_SGT, __lang_gt, MFNONE, dest, left, right); break;
                case EXP_GE: lang_compileBinary(tok, &tright, exp->_ctx._binary, scope, I_SGE, __lang_ge, MFNONE, dest, right, left); break;
                case EXP_EQ: lang_compileBinary(tok, &tright, exp->_ctx._binary, scope, I_SEQ, __lang_eq, MFNONE, dest, left, right); break;
                case EXP_DIFF: lang_compileBinary(tok, &tright, exp->_ctx._binary, scope, I_NEQ, __lang_diff, MFNONE, dest, left, right); break;
        }

        if (!isConst(exp)) { lang_freeReg(tok, scope, left); }
        if (isBinary(exp)) { lang_freeReg(tok, scope, right); }
}

void lang_compileToken(struct T_token** tok, string_type format, ...)
{
        struct T_token* top = (struct T_token*)malloc(sizeof(struct T_token));
        va_list args;

        top->_free._registers = NULL;
        top->_free._size = top->_free._cap = 0;
        top->_label = 0;

        top->_ctx._text = (string_type)malloc(strlen(format) << 1);

        va_start(args, format);
        vsprintf(top->_ctx._text, format, args);
        va_end(args);

        if ((*tok) != NULL) { top->_next = *tok; }
        *tok = top;
}

void lang_compileWriteVariable(struct T_token** tok, struct T_variable* var, struct T_compileSymbols* scope, abstract_reg_type src)
{
        hash_insert(&scope->_symbols, var->_name);
        if ((*tok) != NULL) { (*tok)->_const = 0; }
}

void lang_compileConst(struct T_token** tok, struct T_constant* cons, struct T_compileSymbols* scope)
{
        struct T_token* aux = (struct T_token*)malloc(sizeof(struct T_token));

        aux->_free._registers = NULL;
        aux->_free._size = aux->_free._cap = 0;
        aux->_label = 0;

        aux->_next = *tok;
        aux->_const = 1;
        aux->_ctx._value = cons->_value;
        *tok = aux;
}

void lang_compileLabel(struct T_token** tok, struct T_compileSymbols* scope, abstract_label_type label)
{
        lang_compileToken(tok, "__l%d:", label);
        (*tok)->_label = 1;
}

abstract_reg_type lang_makeReg(struct T_token** tok, struct T_compileSymbols* scope)
{
        string_type str = (string_type)malloc(1 + (scope->_reg == 0 ? 1 : ((int)log10(scope->_reg)) + 1));    // number of digits
        sprintf(str, "%d", scope->_reg);

        hash_insert(&scope->_reserved, str);

        return scope->_reg++;
}

void lang_freeReg(struct T_token** tok, struct T_compileSymbols* scope, abstract_reg_type reg)
{
        string_type str = (string_type)malloc(1 + (reg == 0 ? 1 : ((int)log10(scope->_reg)) + 1));    // number of digits
        sprintf(str, "%d", reg);

        if ((*tok)->_free._cap == (*tok)->_free._size)
        {
                if ((*tok)->_free._cap == 0)
                {
                        (*tok)->_free._registers = (abstract_reg_type*)malloc(sizeof(abstract_reg_type));
                        (*tok)->_free._cap = 1;
                }
                else
                {
                        (*tok)->_free._cap <<= 1;
                        (*tok)->_free._registers = (abstract_reg_type*)realloc((*tok)->_free._registers, sizeof(abstract_reg_type) * (*tok)->_free._cap);
                }
        }

        (*tok)->_free._registers[(*tok)->_free._size++] = reg;

        // TODO remove the hash. It's not being needed
        hash_remove(&scope->_reserved, str);
}

abstract_label_type lang_makeLabel(struct T_token** tok, struct T_compileSymbols* scope)
{
        return scope->_label++;
}

void lang_compileBinary_i(
        struct T_token** tok,
        struct T_token** tright,
        struct T_binary* exp,
        struct T_compileSymbols* scope,
        string_type op,
        int (*app)(int, int),
        enum T_movefrom mv,
        abstract_reg_type dest,
        abstract_reg_type left,
        abstract_reg_type right
)
{
        int imm;

        if ((*tok)->_const)
        {
                imm = (*tok)->_ctx._value;

                // Both are constant, just apply the operation and return
                if ((*tright)->_const)
                {
                        (*tok)->_ctx._value = (*app)((*tok)->_ctx._value, (*tright)->_ctx._value);
                }

                // appi $dest, $s, left (reversed)
                else
                {
                        *tok = (*tok)->_next;   // Pop the constant
                        lang_mergeTokens(tok, tright);

                        switch (mv)
                        {
                                case MFHI:
                                        lang_compileToken(tok, "%si %%r%d, %d", op, right, imm);
                                        lang_compileToken(tok, "mfhi %%r%d", dest);
                                break;

                                case MFLO:
                                        lang_compileToken(tok, "%si, %%r%d, %d", op, right, imm);
                                        lang_compileToken(tok, "mflo %%r%d", dest);
                                break;

                                case MFNONE:
                                        lang_compileToken(tok, "%si %%r%d, %%r%d, %d", op, dest, right, imm);
                                break;
                        }
                }
        }
        else
        {
                // appi $dest, $s, right
                if ((*tright)->_const)
                {
                        switch (mv)
                        {
                                case MFHI:
                                        lang_compileToken(tok, "%si %%r%d, %d", op, left, (*tright)->_ctx._value);
                                        lang_compileToken(tok, "mfhi %%r%d", dest);
                                break;

                                case MFLO:
                                        lang_compileToken(tok, "%si %%r%d, %d", op, left, (*tright)->_ctx._value);
                                        lang_compileToken(tok, "mflo %%r%d", dest);
                                break;

                                case MFNONE:
                                        lang_compileToken(tok, "%si %%r%d, %%r%d, %d", op, dest, left, (*tright)->_ctx._value);
                                break;
                        }
                }

                // app $dest, $s, $r
                else
                {
                        lang_mergeTokens(tok, tright);

                        switch (mv)
                        {
                                case MFHI:
                                        lang_compileToken(tok, "%s %%r%d, %%r%d", op, left, right);
                                        lang_compileToken(tok, "mfhi %%r%d", dest);
                                break;

                                case MFLO:
                                        lang_compileToken(tok, "%s %%r%d, %%r%d", op, left, right);
                                        lang_compileToken(tok, "mflo %%r%d", dest);
                                break;

                                case MFNONE:
                                        lang_compileToken(tok, "%s %%r%d, %%r%d, %%r%d", op, dest, left, right);
                                break;
                        }
                }
        }

        //lang_freeReg(tok, scope, left);
        //lang_freeReg(tok, scope, right);
}

void lang_compileBinary(
        struct T_token** tok,
        struct T_token** tright,
        struct T_binary* exp,
        struct T_compileSymbols* scope,
        string_type op,
        int (*app)(int, int),
        enum T_movefrom mv,
        abstract_reg_type dest,
        abstract_reg_type left,
        abstract_reg_type right
)
{
        int imm;

        if ((*tok)->_const)
        {
                imm = (*tok)->_ctx._value;

                // Both are constant, just apply the operation and return
                if ((*tright)->_const) { (*tok)->_ctx._value = (*app)((*tok)->_ctx._value, (*tright)->_ctx._value); }

                /*
                 * li $s, left
                 * app $dest, $s, $r
                 */
                else
                {
                        *tok = (*tok)->_next;   // Pop the constant
                        lang_mergeTokens(tok, tright);

                        switch (mv)
                        {
                                case MFHI:
                                        lang_compileToken(tok, "li %%r%d, %d", left, imm);
                                        lang_compileToken(tok, "%s %%r%d, %%r%d", op, left, right);
                                        lang_compileToken(tok, "mfhi %%r%d", dest);
                                break;

                                case MFLO:
                                        lang_compileToken(tok, "li %%r%d, %d", left, imm);
                                        lang_compileToken(tok, "%s %%r%d, %%r%d", op, left, right);
                                        lang_compileToken(tok, "mflo %%r%d", dest);
                                break;

                                case MFNONE:
                                        lang_compileToken(tok, "li %%r%d, %d", left, imm);
                                        lang_compileToken(tok, "%s %%r%d, %%r%d, %%r%d", op, dest, left, right);
                                break;
                        }
                }
        }
        else
        {
                // li $s, right
                // app $dest, $r, $s
                if ((*tright)->_const)
                {
                        switch (mv)
                        {
                                case MFHI:
                                        lang_compileToken(tok, "li %%r%d, %d", right, (*tright)->_ctx._value);
                                        lang_compileToken(tok, "%s %%r%d, %%r%d", op, left, right);
                                        lang_compileToken(tok, "mfhi %%r%d", dest);
                                break;

                                case MFLO:
                                        lang_compileToken(tok, "li %%r%d, %d", right, (*tright)->_ctx._value);
                                        lang_compileToken(tok, "%s %%r%d, %%r%d", op, left, right);
                                        lang_compileToken(tok, "mflo %%r%d", dest);
                                break;

                                case MFNONE:
                                        lang_compileToken(tok, "li %%r%d, %d", right, (*tright)->_ctx._value);
                                        lang_compileToken(tok, "%s %%r%d, %%r%d, %%r%d", op, dest, left, right);
                                break;
                        }
                }

                // app $dest, $r, $s
                else
                {
                        lang_mergeTokens(tok, tright);

                        switch (mv)
                        {
                                case MFHI:
                                        lang_compileToken(tok, "%s %%r%d, %%r%d", op, left, right);
                                        lang_compileToken(tok, "mfhi %%r%d", dest);
                                break;

                                case MFLO:
                                        lang_compileToken(tok, "%s %%r%d, %%r%d", op, left, right);
                                        lang_compileToken(tok, "mfhi %%r%d", dest);
                                break;

                                case MFNONE:
                                        lang_compileToken(tok, "%s %%r%d, %%r%d, %%r%d", op, dest, left, right);
                                break;
                        }
                }
        }

        //lang_freeReg(tok, scope, left);
        //lang_freeReg(tok, scope, right);
}

void lang_forceConstLoad(struct T_token** tok, abstract_reg_type reg)
{
        struct T_token* top = *tok;

        if (top->_const)
        {
                *tok = top->_next;    // Pop

                lang_compileToken(tok, "li %%r%d, %d", reg, top->_ctx._value);

                free(top);
        }
}

void lang_mergeTokens(struct T_token** tleft, struct T_token** tright)
{
        if (*tright != NULL)
        {
                lang_mergeTokens(tleft, &(*tright)->_next);
                (*tright)->_next = *tleft;
                *tleft = *tright;
        }
}

int __lang_add(int left, int right)  { return left + right; }
int __lang_and(int left, int right)  { return left & right; }
int __lang_or(int left, int right)   { return left | right; }
int __lang_xor(int left, int right)  { return left ^ right; }
int __lang_slt(int left, int right)  { return left < right; }
int __lang_div(int left, int right)  { return left / right; }
int __lang_mod(int left, int right)  { return left % right; }
int __lang_mult(int left, int right) { return left * right; }
int __lang_sub(int left, int right)  { return left - right; }
int __lang_not(int value)            { return !value; }
int __lang_symmetric(int value)      { return -value; }
int __lang_eq(int left, int right)   { return left == right; }
int __lang_le(int left, int right)   { return left <= right; }
int __lang_gt(int left, int right)   { return left > right; }
int __lang_ge(int left, int right)   { return left >= right; }
int __lang_diff(int left, int right) { return left != right; }

