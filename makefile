MAIN=lang

GRAMMER=bison
LEXER=flex
CC=gcc

GRAMMER_FLAGS=-d
LEXER_FLAGS=
CC_FLAGS=-lm -Werror

$(MAIN): lang.lex lang.y langdefs.c langdefs.h langc.c langc.h hash.c hash.h heap.c heap.h lang.c
	$(GRAMMER) $(GRAMMER_FLAGS) lang.y
	$(LEXER) $(LEXER_FLAGS) lang.lex
	$(CC) $(CC_FLAGS) *.c -o $(MAIN)

test: $(MAIN)
	./lang in -o out
	xspim out
	rm out

clean:
	rm -f lang.tab.c lang.tab.h lex.yy.c

