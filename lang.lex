%{
#include <string.h>
#include <stdio.h>
#include "langdefs.h"
#include "lang.tab.h"

int yylineno = 0;
%}
%option noyywrap

manglename      __l[0-9]+|"main"|"__nl"
name            [_a-zA-Z][_a-zA-Z0-9]*
newline         "\r""\n"?|"\n"
ws              [ \t\v]+
comment         "//".*?{newline}|"/*"(?:.*?{newline}?)*"*/"
integer         [1-9][0-9]*|[0]

%%
{comment}	{}

";"             { return ENDBLOCK; }

"if"            { return IF; }
"then"          { return THEN; }
"else"          { return ELSE; }
"endif"         { return ENDIF; }

"read"          { return READ; }
"write"         { return WRITE; }

"repeat"        { return REPEAT; }
"until"         { return UNTIL; }

"=="            { return EQ; }
"!="            { return DIFF; }
">="            { return GE; }
"<="            { return LE; }
">"             { return GT; }
"<"             { return LT; }

"*"             { return MULT; }
"/"             { return DIV; }
"%"             { return MOD; }
"+"             { return PLUS; }
"-"             { return MINUS; }

"!"             { return NOT; }
"&&"            { return AND; }
"||"            { return OR; }

"~"             { return BNEG; }
"&"             { return BAND; }
"|"             { return BOR; }
"^"             { return BXOR; }

"="             { return ASSIGN; }

"("             { return OPENP; }
")"             { return CLOSEP; }

{integer}       { yylval.number_t = atoi(yytext); return NUMBER; }
{manglename}    { yylval.string_t = yytext; yylval.string_t[0] = 'v'; return NAME; }
{name}          { yylval.string_t = yytext; return NAME; }
{newline}       { ++yylineno; }
{ws}		{}
%%

void yyerror(const char* pmsg)
{
        extern int yylineno;
        extern char* yytext;

        fprintf(stderr, "ERROR: %s at symbol '%s' line %d\n", pmsg, yytext, yylineno);
}

void yyerrorl(const char* pmsg, int lineno)
{
        fprintf(stderr, "ERROR: %s at line %d\n", pmsg, lineno);
}

void yyerrors(const char* pmsg, const char* psymbol, int lineno)
{
        fprintf(stderr, "ERROR: %s '%s' at line %d\n", pmsg, psymbol, lineno);
}


