%{
#include <stdlib.h>
#include "langdefs.h"

size_type __lang_err_count;
struct T_block* __lang_parse_result = NULL;

struct T_block* lang_parse();

%}
%union {
	struct T_exp* exp_t;
        struct T_state* state_t;
        struct T_block* block_t;
        value_type number_t;
        string_type string_t;
}
%token ENDBLOCK
%token IF THEN ELSE ENDIF
%token READ WRITE
%token REPEAT UNTIL
%token EQ DIFF
%token GE LE GT LT
%token MULT DIV MOD PLUS MINUS
%token NOT AND OR
%token BNEG BAND BOR BXOR
%token ASSIGN
%token OPENP CLOSEP
%token <number_t> NUMBER
%token <string_t> NAME
%type <exp_t> variable atom factor term purearith band bxor arithmetic comp eq and expression not
%type <state_t> repeatstate ifstate readstate writestate assignment statement loosestatement
%type <block_t> block lang
%%
lang:                   { __lang_err_count = 0; __lang_parse_result = NULL; }
                        block
                        { $$ = __lang_parse_result = $2; }
;

block:                  /* empty */                                             { $$ = NULL; }
                        | statement block                                       { $$ = makeBlock($1, $2); }
;

statement:              loosestatement ENDBLOCK                                 { $$ = $1; }
;

loosestatement:         repeatstate                                             { $$ = $1; }
                        | ifstate                                               { $$ = $1; }
                        | readstate                                             { $$ = $1; }
                        | writestate                                            { $$ = $1; }
                        | assignment                                            { $$ = $1; }
;

repeatstate:            REPEAT block UNTIL expression                           { $$ = makeState(STATE_REPEAT, $2, $4); assert_t($4->_vt == TYPE_BOOL); }
;

ifstate:                IF expression THEN block ELSE block ENDIF               {
                                                                                        $$ = makeState(STATE_IF, $2, $4, $6);
                                                                                        assert_tm($2->_vt == TYPE_BOOL, "invalid conditional expression");
                                                                                }
                        |IF expression THEN block ENDIF                         {
                                                                                        $$ = makeState(STATE_IF, $2, $4, NULL);
                                                                                        assert_tm($2->_vt == TYPE_BOOL, "invalid conditional expression");
                                                                                }
;

readstate:              READ variable                                           { $$ = makeState(STATE_READ, $2); }
;

writestate:             WRITE expression                                        { $$ = makeState(STATE_WRITE, $2); assert_t($2->_vt == TYPE_INT); }
;

assignment:             variable ASSIGN arithmetic                              { $$ = makeState(STATE_ASSIGN, $1, $3); assert_t($3->_vt == TYPE_INT); }
;

expression:             expression OR and                                       {
                                                                                        $$ = makeBinaryExp(EXP_OR, $1, $3, TYPE_BOOL);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_BOOL);
                                                                                }
                        | and                                                   { $$ = $1; }
;

and:                    and AND not                                             {
                                                                                        $$ = makeBinaryExp(EXP_AND, $1, $3, TYPE_BOOL);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_BOOL);
                                                                                }
                        | not                                                   { $$ = $1; }
;

not:                    NOT eq                                                  { $$ = makeUnaryExp(EXP_NOT, $2, TYPE_BOOL); assert_t($2->_vt == TYPE_BOOL); }
                        | eq                                                    { $$ = $1; }
;

eq:                     eq EQ comp                                              {
                                                                                        $$ = makeBinaryExp(EXP_EQ, $1, $3, TYPE_BOOL);
                                                                                        assert_t($1->_vt == $3->_vt);
                                                                                }
                        | eq DIFF comp                                          {
                                                                                        $$ = makeBinaryExp(EXP_DIFF, $1, $3, TYPE_BOOL);
                                                                                        assert_t($1->_vt == $3->_vt);
                                                                                }
                        | comp                                                  { $$ = $1; }
;

comp:                   comp LT arithmetic                                      {
                                                                                        $$ = makeBinaryExp(EXP_LT, $1, $3, TYPE_BOOL);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | comp LE arithmetic                                    {
                                                                                        $$ = makeBinaryExp(EXP_LE, $1, $3, TYPE_BOOL);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | comp GT arithmetic                                    {
                                                                                        $$ = makeBinaryExp(EXP_GT, $1, $3, TYPE_BOOL);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | comp GE arithmetic                                    {
                                                                                        $$ = makeBinaryExp(EXP_GE, $1, $3, TYPE_BOOL);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | arithmetic                                            { $$ = $1; }
;

arithmetic:             arithmetic BOR bxor                                     {
                                                                                        $$ = makeBinaryExp(EXP_BOR, $1, $3, TYPE_INT);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | bxor                                                  { $$ = $1; }
;

bxor:                   bxor BXOR band                                          {
                                                                                        $$ = makeBinaryExp(EXP_BXOR, $1, $3, TYPE_INT);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | band                                                  { $$ = $1; }
;

band:                   band BAND purearith                                     {
                                                                                        $$ = makeBinaryExp(EXP_BAND, $1, $3, TYPE_INT);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | purearith                                             { $$ = $1; }
;

purearith:              purearith PLUS term                                     {
                                                                                        $$ = makeBinaryExp(EXP_ADD, $1, $3, TYPE_INT);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | purearith MINUS term                                  {
                                                                                        $$ = makeBinaryExp(EXP_SUBTRACT, $1, $3, TYPE_INT);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | term                                                  { $$ = $1; }
;

term:                   term MULT factor                                        { $$ = makeBinaryExp(EXP_MULT, $1, $3, TYPE_INT); }
                        | term DIV factor                                       {
                                                                                        $$ = makeBinaryExp(EXP_DIV, $1, $3, TYPE_INT);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | term MOD factor                                       {
                                                                                        $$ = makeBinaryExp(EXP_MOD, $1, $3, TYPE_INT);
                                                                                        assert_t($1->_vt == $3->_vt && $3->_vt == TYPE_INT);
                                                                                }
                        | factor                                                { $$ = $1; }
;

factor:                 PLUS factor                                             { $$ = $2; assert_t($2->_vt == TYPE_INT); /* Identity */ }
                        | MINUS factor                                          { $$ = makeUnaryExp(EXP_SYMMETRIC, $2, TYPE_INT); assert_t($2->_vt == TYPE_INT); }
                        | BNEG factor                                           { $$ = makeUnaryExp(EXP_BNEG, $2, TYPE_INT); assert_t($2->_vt == TYPE_INT); }
                        | OPENP expression CLOSEP                               { $$ = $2; }
                        | atom                                                  { $$ = $1; }
;

atom:                   variable                                                { $$ = $1; extern int yylineno; $1->_ctx._variable->_lineno = yylineno; }
                        | NUMBER                                                { $$ = makeUnaryExp(EXP_CONSTANT, &$1, TYPE_INT); }
;

variable:               NAME                                                    { $$ = makeUnaryExp(EXP_VARIABLE, $1, TYPE_INT); }
;

%%
struct T_block* lang_parse()
{
        return yyparse() == 0 ? __lang_parse_result : NULL;
}

