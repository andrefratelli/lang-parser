#include "lang.h"

struct T_options lang_parseOptions(int argc, char** argv)
{
        struct T_options options;
        size_type it;
        int state = 0;
        string_type _in = NULL;
        string_type _asm = NULL;
        string_type _tree = NULL;

        options._in = NULL;
        options._asm = NULL;
        options._tree = NULL;
        options._valid = 0;

        for (it=1 ; it<argc ; it++)
        {
                switch (state)
                {
                        /*
                         * Expecting: "file", "-o" or "-t"
                         */
                        case 0:
                                if (strcmp(argv[it], "-o") == 0)
                                {
                                        if (_asm == NULL) { state = 1; }
                                        else { lang_out_syntax(NULL); return options; }
                                }
                                else if (strcmp(argv[it], "-t") == 0)
                                {
                                        if (_tree == NULL) { state = 2; }
                                        else { lang_out_syntax(NULL); return options; }
                                }
                                else if (argv[it][0] != '-')
                                {
                                        if (_in == NULL) { _in = argv[it]; }
                                        else { lang_out_syntax(NULL); return options; }
                                }
                                else { lang_out_syntax(argv[it]); return options; }
                        break;

                        /*
                         * Expecting: -o "file"
                         */
                        case 1:
                                if (argv[it][0] != '-') { _asm = argv[it]; state = 0; }
                                else { lang_out_syntax(argv[it]); return options; }
                        break;

                        /*
                         * Expecting: -t "file"
                         */
                        case 2:
                                if (argv[it][0] != '-') { _tree = argv[it]; state = 0; }
                                else { lang_out_syntax(argv[it]); return options; }
                        break;
                }
        }

        switch (state)
        {
                case 1:
                case 2:
                        lang_out_syntax(NULL);
                        return options;
                break;
        }

        if (_in == NULL) { options._in = stdin; options._stdin = 1; }
        else
        {
                options._in = freopen(_in, "r", stdin);
                options._stdin = 0;

                if (options._in == NULL)
                {
                        fprintf(stderr, "cannot open file: %s\n", _in);
                        return options;
                }
        }

        if (_asm == NULL) { options._asm = stdout; }
        else
        {
                options._asm = fopen(_asm, "w");
                if (options._asm == NULL)
                {
                        fprintf(stderr, "cannot open file: %s\n", _asm);

                        options._in = NULL;
                        return options;
                }
        }

        if (_tree != NULL)
        {
                options._tree = fopen(_tree, "w");
                if (options._tree == NULL)
                {
                        fprintf(stderr, "cannot open file: %s\n", _tree);

                        options._in = NULL;
                        options._asm = NULL;
                        return options;
                }
        }

        options._valid = 1;

        return options;
}

void lang_out_syntax(string_type arg)
{
        if (arg == NULL) { fprintf(stderr, "syntax: lang input [-o asm] [-t tree]\n"); }
        else { fprintf(stderr, "invalid option: %s\n", arg); }
}

void lang_out_depth(FILE* stream, size_type depth)
{
        while (depth-- > 0) { fprintf(stream, "\t"); }
}

void lang_out_tree(FILE* stream, struct T_block* block)
{
        while (block != NULL)
        {
                lang_out_state(stream, block->_state, 0);
                block = block->_next;
        }
}

void lang_out_block(FILE* stream, struct T_block* block, size_type depth)
{
        while (block != NULL)
        {
                lang_out_state(stream, block->_state, depth + 1);
                block = block->_next;
        }
}

void lang_out_exp(FILE* stream, struct T_exp* exp, size_type depth)
{
        string_type types[] = {
                "VAR", "CONST", "SYMM", "MOD", "DIV",
                "MULT", "SUB", "ADD", "BNEG", "BAND",
                "BXOR", "BOR", "LT", "LE", "GT",
                "GE", "AND", "OR", "NOT", "EQ", "DIFF"
        };

        string_type ops[] = {
                "var", "", "-", "%", "/",
                "*", "-", "+", "~", "&",
                "^", "|", "<", "<=", ">",
                ">=", "&&", "||", "!", "==", "!="
        };

        if (isConst(exp))
        {
                lang_out_depth(stream, depth);
                fprintf(stream, "(CONST %d)\n", exp->_ctx._constant->_value);
        }
        else if (isVar(exp))
        {
                lang_out_depth(stream, depth);
                fprintf(stream, "(VAR %s)\n", exp->_ctx._variable->_name);
        }
        else if (isBinary(exp))
        {
                lang_out_depth(stream, depth);
                fprintf(stream, "(%s %s)\n", types[exp->_type], ops[exp->_type]);

                lang_out_depth(stream, depth);
                fprintf(stream, "[left]\n");
                lang_out_exp(stream, exp->_ctx._binary->_left, depth + 1);

                lang_out_depth(stream, depth);
                fprintf(stream, "[right]\n");
                lang_out_exp(stream, exp->_ctx._binary->_right, depth + 1);
        }
}

void lang_out_state(FILE* stream, struct T_state* exp, size_type depth)
{
        switch (exp->_type)
        {
                case STATE_REPEAT:
                        lang_out_depth(stream, depth);
                        fprintf(stream, "(REPEAT repeat)\n");
                        lang_out_block(stream, exp->_ctx._repeat->_block, depth + 1);

                        lang_out_depth(stream, depth);
                        fprintf(stream, "(UNTIL until)\n");
                        lang_out_exp(stream, exp->_ctx._repeat->_exp, depth + 1);
                break;

                case STATE_IF:
                        lang_out_depth(stream, depth);
                        fprintf(stream, "(IF if)\n");
                        lang_out_exp(stream, exp->_ctx._if->_exp, depth + 1);

                        lang_out_depth(stream, depth);
                        fprintf(stream, "(IF block)\n");
                        lang_out_block(stream, exp->_ctx._if->_block, depth + 1);

                        lang_out_depth(stream, depth);
                        fprintf(stream, "(ELSE block)\n");
                        lang_out_block(stream, exp->_ctx._if->_fail, depth + 1);
                break;

                case STATE_READ:
                        lang_out_depth(stream, depth);
                        fprintf(stream, "(READ %s)\n", exp->_ctx._read->_exp->_name);
                break;

                case STATE_WRITE:
                        lang_out_depth(stream, depth);
                        fprintf(stream, "(WRITE write)\n");
                        lang_out_exp(stream, exp->_ctx._write->_exp, depth + 1);
                break;

                case STATE_ASSIGN:
                        lang_out_depth(stream, depth);
                        fprintf(stream, "(ASSIGN %s)\n", exp->_ctx._assign->_lval->_name);
                        lang_out_exp(stream, exp->_ctx._assign->_rval, depth + 1);
                break;
        }
}
/*
void lang_out_var(FILE* stream, struct T_variable* var, size_type depth) {}
void lang_out_const(FILE* stream, struct T_constant* cons, size_type depth) {}
void lang_out_binary(FILE* stream, struct T_binary* bin, size_type depth) {}
void lang_out_unary(FILE* stream, struct T_unary* un, size_type depth) {}
*/
void lang_out_symbols(FILE* stream, struct T_compileSymbols scope)
{
        size_type it, jt;

        fprintf(stream, "\t\t.data\n");
        fprintf(stream, "%s:\t\t.asciiz \"\\n\"\n", LANG_NEWLINE);
        for (it=0 ; it<scope._symbols._size ; it++)
        {
                for (jt=0 ; jt<scope._symbols._sections[it]._size ; jt++)
                {
                        fprintf(stream, "%s:\t\t.word 0\n", scope._symbols._sections[it]._heap[jt + 1]);
                }
        }
}

void lang_out_asm_iter(FILE* stream, struct T_asm* src, struct T_compileSymbols scope)
{
        if (src != NULL)
        {
                lang_out_asm_iter(stream, src->_next, scope);

                if (src->_label) { fprintf(stream, "%s\n", src->_token); }
                else { fprintf(stream, "\t\t%s\n", src->_token); }
        }
}

void lang_out_asm(FILE* stream, struct T_asm* src, struct T_compileSymbols scope)
{
        fprintf(stream, "\t\t.text\n");
        fprintf(stream, LANG_MAIN);
        lang_out_asm_iter(stream, src, scope);

        // Write program termination (exit)
        fprintf(stream, "\t\tli $v0, 10\n");
        fprintf(stream, "\t\tsyscall\n");
}

/*
 * usage
 *
 * lang in [-o out] [-t tree]
 */
int main(int argc, char** argv)
{
        struct T_options options = lang_parseOptions(argc, argv);
        struct T_block* block;
        struct T_compileSymbols scope;
        struct T_asm* src;

        if (options._valid)
        {
                block = lang_parse();

                if (block != NULL)
                {
                        if (options._tree != NULL) { lang_out_tree(options._tree, block); }

                        hash_make(&scope._symbols, 10);
                        hash_make(&scope._reserved, 10);

                        src = lang_compile(block, &scope);

                        if (options._asm)
                        {
                                lang_out_symbols(options._asm, scope);
                                lang_out_asm(options._asm, src, scope);
                        }

                        hash_destroy(&scope._symbols);
                        hash_destroy(&scope._reserved);

                        if (options._in != stdin) { fclose(options._in); }
                        if (options._asm != stdout) { fclose(options._asm); }
                        if (options._tree != NULL) { fclose(options._tree); }

                        return 0;
                }
        }

        return 1;
}

