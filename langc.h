#ifndef __LANGC_H
#define __LANGC_H

#include "langdefs.h"
#include "hash.h"

extern struct T_block* lang_parse();

#define LANG_MAIN "main:"
#define LANG_NEWLINE "__nl"

/*
 * Arithmetic Operations
 */
#define I_ADD "add"
#define I_ADDI "addi"
#define I_ADDIU "addiu"
#define I_ADDU "addu"
#define I_CLO "clo"
#define I_CLZ "clz"
#define I_LA "la"
#define I_LI "li"
#define I_LUI "lui"
#define I_MOVE "move"
#define I_NEGU "negu"
#define I_SEB "seb"
#define I_SEH "seh"
#define I_SUB "sub"
#define I_SUBU "subu"

/*
 * Shift and Rotate Operations
 */
#define I_ROTR "rotr"
#define I_ROTRV "rotrv"
#define I_SLL "sll"
#define I_SLLV "sllv"
#define I_SRA "sra"
#define I_SRAV "srav"
#define I_SRLV "srlv"

/*
 * Logical and Bit-Field Operations
 */
#define I_AND "and"
#define I_ANDI "andi"
#define I_EXT "ext"
#define I_INS "ins"
#define I_NOP "nop"
#define I_NOR "nor"
#define I_NOT "not"
#define I_OR "or"
#define I_ORI "ori"
#define I_WSBH "wsbh"
#define I_XOR "xor"
#define I_XORI "xori"

/*
 * Condition Testing and Conditional Move Operations
 */
#define I_MOVN "movn"
#define I_MOVZ "movz"
#define I_SLT "slt"
#define I_SLTI "slti"
#define I_SLTIU "sltiu"
#define I_SLTU "sltu"

/*
 * Multiply and Divide Operations
 */
#define I_DIV "div"
#define I_DIVU "divu"
#define I_MADD "madd"
#define I_MADDU "maddu"
#define I_MSUB "msub"
#define I_MSUBU "msubu"
#define I_MUL "mul"
#define I_MULT "mult"
#define I_MULTU "multu"

/*
 * Accumulator Access Operations
 */
#define I_MFHI "mfhi"
#define I_MFLO "mflo"
#define I_MTHI "mthi"
#define I_MTLO "mtlo"

/*
 * Jumps and Branches
 */
#define I_B "b"
#define I_BAL "bal"
#define I_BEQ "beq"
#define I_BEQZ "beqz"
#define I_BGEZ "bgez"
#define I_BGEZAL "bgezal"
#define I_BGTZ "bgtz"
#define I_BLEZ "blez"
#define I_BLTZ "bltz"
#define I_BLTZAL "bltzal"
#define I_BNE "bne"
#define I_BNEZ "bnez"
#define I_J "j"
#define I_JAL "jal"
#define I_JALR "jalr"
#define I_JR "jr"

/*
 * Load and Store Operations
 */
#define I_LB "lb"
#define I_LBU "lbu"
#define I_LH "lh"
#define I_LHU "lhu"
#define I_LW "lw"
#define I_LWL "lwl"
#define I_LWR "lwr"
#define I_SB "sb"
#define I_SH "sh"
#define I_SW "sw"
#define I_SWL "swl"
#define I_SWR "swr"
#define I_ULW "ulw"
#define I_USW "usw"

/*
 * Extended
 */
#define I_SGT "sgt"
#define I_SLE "sle"
#define I_SGE "sge"
#define I_SEQ "seq"
#define I_NEQ "neq"

#ifndef __REG_TYPE__
#define __REG_TYPE__
typedef unsigned int abstract_reg_type;
typedef unsigned int abstract_label_type;
typedef abstract_reg_type reg_type;
#endif

#define REGISTER_COUNT 10

struct T_compileSymbols {
        abstract_reg_type _reg;
        abstract_label_type _label;
	struct hash _symbols;
        struct hash _reserved;
        reg_type _registers[REGISTER_COUNT];
};

struct reg_vector {
        abstract_reg_type* _registers;
        size_type _size;
        size_type _cap;
};

struct T_token {
        union {
                string_type _text;
                int _value;
        } _ctx;
        struct T_token* _next;
        int _const;
        struct reg_vector _free;
        bool_type _label;
};

enum T_movefrom {
        MFHI,
        MFLO,
        MFNONE
};

struct T_asm {
        string_type _token;
        struct T_asm* _next;
        bool_type _label;
};

string_type lang_map_registers(string_type token, struct hash_map* map);
string_type lang_resolve_registers(struct T_token* tok, struct T_compileSymbols* scope, struct hash_map* map, string_type token);

void __lang_parse_reg(string_type str);
string_type __lang_parse_next_reg();

struct T_asm* lang_compile_asm(struct T_token* tok, struct T_compileSymbols* scope, struct hash_map* map);
struct T_asm* lang_compile(struct T_block* block, struct T_compileSymbols* scope);
void lang_compileBlock(struct T_token** tok, struct T_block* block, struct T_compileSymbols* symbols);
void lang_compileStatement(struct T_token** tok, struct T_state* state, struct T_compileSymbols* symbols);

void lang_compileExpression(struct T_token** tok, struct T_exp* exp, struct T_compileSymbols* symbols, abstract_reg_type dest);
void lang_compileToken(struct T_token** tok, string_type format, ...);
void lang_compileWriteVariable(struct T_token** tok, struct T_variable* var, struct T_compileSymbols* symbols, abstract_reg_type src);
void lang_compileConst(struct T_token** tok, struct T_constant* cons, struct T_compileSymbols* symbols);

void lang_compileBinary_i(
        struct T_token** tok,
        struct T_token** tright,
        struct T_binary* exp,
        struct T_compileSymbols* symbols,
        string_type op,
        int (*app)(int, int),
        enum T_movefrom mv,
        abstract_reg_type dest,
        abstract_reg_type left,
        abstract_reg_type right
);
void lang_compileBinary(
        struct T_token** tok,
        struct T_token** tright,
        struct T_binary* exp,
        struct T_compileSymbols* symbols,
        string_type op,
        int (*app)(int, int),
        enum T_movefrom mv,
        abstract_reg_type dest,
        abstract_reg_type left,
        abstract_reg_type right
);

void lang_compileLabel(struct T_token**, struct T_compileSymbols* symbols, abstract_label_type label);

void lang_forceConstLoad(struct T_token** tok, abstract_reg_type reg);

void lang_mergeTokens(struct T_token** tleft, struct T_token** tright);

abstract_reg_type lang_makeReg(struct T_token** tok, struct T_compileSymbols* symbols);
void lang_freeReg(struct T_token** tok, struct T_compileSymbols* symbols, abstract_reg_type reg);

abstract_label_type lang_makeLabel(struct T_token** tok, struct T_compileSymbols* symbols);

int __lang_add(int left, int right);
int __lang_and(int left, int right);
int __lang_or(int left, int right);
int __lang_xor(int left, int right);
int __lang_slt(int left, int right);
int __lang_div(int left, int right);
int __lang_mod(int left, int right);
int __lang_mult(int left, int right);
int __lang_sub(int left, int right);
int __lang_not(int value);
int __lang_symmetric(int value);
int __lang_eq(int left, int right);
int __lang_le(int left, int right);
int __lang_gt(int left, int right);
int __lang_ge(int left, int right);
int __lang_diff(int left, int right);
#endif

