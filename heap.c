#include "heap.h"
#include <stdlib.h>
#include <string.h>

void heap_make(struct heap* h)
{
        h->_heap = NULL;
        h->_size = 0;
        h->_cap = 0;
}

void heap_destroy(struct heap* h)
{
        if (h->_heap != NULL) {
                free(h->_heap);
                h->_heap = NULL;
        }
        h->_size = 0;
        h->_cap = 0;
}

size_type heap_find(struct heap* h, string_type string)
{
        size_type index;

        for (index=1 ; index<h->_size + 1 ; index++)
        {
                if (strcmp(h->_heap[index], string) == 0) { return index;}
        }

        return HEAP_UNDEF;
}

bool_type heap_contains(struct heap* h, string_type string)
{
        return heap_find(h, string) != HEAP_UNDEF;
}

void heap_push(struct heap* h, string_type string)
{
        if (heap_contains(h, string)) { return; }

        if (h->_size == h->_cap)
        {
                if (h->_cap == 0)
                {
                        h->_cap = INITIAL_HEAP_SIZE;
                        h->_heap = (string_type*)malloc(h->_cap);
                        h->_heap[0] = NULL;
                }
                else
                {
                        h->_cap <<= 1;
                        h->_heap = (string_type*)realloc(h->_heap, h->_cap * sizeof(string_type));
                }
        }

        string_type cpy = (string_type)malloc(strlen(string));
        strcpy(cpy, string);

        h->_heap[h->_size + 1] = cpy;

        size_type it = h->_size;
        size_type parent = it >> 1;
        string_type aux;

        while (parent > 0)
        {
                if (strcmp(h->_heap[it], h->_heap[parent]) < 0)
                {
                        aux = h->_heap[it];
                        h->_heap[it] = h->_heap[parent];
                        h->_heap[parent] = aux;

                        it = parent;
                        parent = it >> 1;
                }
                else { break; }
        }

        h->_size++;
}

void heap_remove(struct heap* h, string_type string)
{
        size_type index = heap_find(h, string);
        string_type aux;
        size_type left, right;

        if (index != HEAP_UNDEF)
        {
                // Swap with last
                aux = h->_heap[index];
                h->_heap[index] = h->_heap[h->_size];
                h->_heap[h->_size] = aux;

                // Pop
                h->_size--;

                // Perc down
                while (1)
                {
                        left = index << 1;
                        right = left + 1;

                        if (left < h->_size + 1)
                        {
                                if (strcmp(h->_heap[index], h->_heap[left]) > 0)
                                {
                                        aux = h->_heap[left];
                                        h->_heap[left] = h->_heap[index];
                                        h->_heap[index] = aux;

                                        index = left;
                                }
                                else if (right < h->_size + 1)
                                {
                                        if (strcmp(h->_heap[index], h->_heap[right]) > 0)
                                        {
                                                aux = h->_heap[right];
                                                h->_heap[right] = h->_heap[index];
                                                h->_heap[index] = aux;

                                                index = right;
                                        }
                                        else { return; }
                                }
                                else { return; }
                        }
                        else { return; }
                }
        }
}

size_type heap_size(struct heap* h)
{
        return h->_size;
}

void heap_map_make(struct heap_map* h)
{
        h->_heap = NULL;
        h->_size = 0;
        h->_cap = 0;
}

void heap_map_destroy(struct heap_map* h)
{
        if (h->_heap != NULL) {
                free(h->_heap);
                h->_heap = NULL;
        }
        h->_size = 0;
        h->_cap = 0;
}

size_type heap_map_find(struct heap_map* h, abstract_reg_type _key)
{
        size_type index;

        for (index=1 ; index<h->_size + 1 ; index++)
        {
                if (h->_heap[index]._key == _key) { return index; }
        }

        return HEAP_UNDEF;
}

bool_type heap_map_contains(struct heap_map* h, abstract_reg_type key)
{
        return heap_map_find(h, key) != HEAP_UNDEF;
}

void heap_map_push(struct heap_map* h, struct heap_map_entry entry)
{
        if (heap_map_contains(h, entry._key)) { return; }

        if (h->_size == h->_cap)
        {
                if (h->_cap == 0)
                {
                        h->_cap = INITIAL_HEAP_SIZE;
                        h->_heap = (struct heap_map_entry*)malloc(h->_cap);
                }
                else
                {
                        h->_cap <<= 1;
                        h->_heap = (struct heap_map_entry*)realloc(h->_heap, h->_cap * sizeof(string_type));
                }
        }

        h->_heap[h->_size + 1] = entry;

        size_type it = h->_size;
        size_type parent = it >> 1;
        struct heap_map_entry aux;

        while (parent > 0)
        {
                if (h->_heap[it]._key < entry._key)
                {
                        aux = h->_heap[it];
                        h->_heap[it] = h->_heap[parent];
                        h->_heap[parent] = aux;

                        it = parent;
                        parent = it >> 1;
                }
                else { break; }
        }

        h->_size++;
}

void heap_map_remove(struct heap_map* h, abstract_reg_type _key)
{
        size_type index = heap_map_find(h, _key);
        size_type left, right;
        struct heap_map_entry aux;

        if (index != HEAP_UNDEF)
        {
                // Swap with last
                aux = h->_heap[index];
                h->_heap[index] = h->_heap[h->_size];
                h->_heap[h->_size] = aux;

                // Pop
                h->_size--;

                // Perc down
                while (1)
                {
                        left = index << 1;
                        right = left + 1;

                        if (left < h->_size + 1)
                        {
                                if (h->_heap[index]._key > h->_heap[left]._key)
                                {
                                        aux = h->_heap[left];
                                        h->_heap[left] = h->_heap[index];
                                        h->_heap[index] = aux;

                                        index = left;
                                }
                                else if (right < h->_size + 1)
                                {
                                        if (h->_heap[index]._key > h->_heap[right]._key)
                                        {
                                                aux = h->_heap[right];
                                                h->_heap[right] = h->_heap[index];
                                                h->_heap[index] = aux;

                                                index = right;
                                        }
                                        else { return; }
                                }
                                else { return; }
                        }
                        else { return; }
                }
        }
}

size_type heap_map_size(struct heap_map* h)
{
        return h->_size;
}

