#include "langdefs.h"
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>

struct T_exp* makeUnaryExp(enum exp_type type, void* value, enum type_type vt)
{
        struct T_exp* exp = (struct T_exp*)malloc(sizeof(struct T_exp));
        string_type name;
        value_type* val;
        struct T_exp* aux;

        exp->_type = type;
        exp->_vt = vt;

        switch (type)
        {
                case EXP_VARIABLE:
                        name = (string_type)value;

                        exp->_ctx._variable = (struct T_variable*)malloc(sizeof(struct T_variable));
                        exp->_ctx._variable->_name = (string_type)malloc(strlen(name));
                        strcpy(exp->_ctx._variable->_name, name);
                break;

                case EXP_CONSTANT:
                        val = (value_type*)value;

                        exp->_ctx._constant = (struct T_constant*)malloc(sizeof(struct T_constant));
                        exp->_ctx._constant->_value = *val;
                break;

                case EXP_BNEG:
                case EXP_NOT:
                case EXP_SYMMETRIC:
                        aux = (struct T_exp*)value;

                        exp->_ctx._unary = (struct T_unary*)malloc(sizeof(struct T_unary));
                        exp->_ctx._unary->_exp = aux;
                break;
        }
        return exp;
}

struct T_exp* makeBinaryExp(enum exp_type type, struct T_exp* left, struct T_exp* right, enum type_type vt)
{
        struct T_exp* exp = (struct T_exp*)malloc(sizeof(struct T_exp));

        exp->_type = type;
        exp->_ctx._binary = (struct T_binary*)malloc(sizeof(struct T_binary));
        exp->_ctx._binary->_left = left;
        exp->_ctx._binary->_right = right;
        exp->_vt = vt;

        return exp;
}

struct T_state* makeState(enum state_type type, ...)
{
        struct T_state* state = (struct T_state*)malloc(sizeof(struct T_state));
        state->_type = type;

        va_list argp;
        va_start(argp, type);

        switch (type)
        {
                case STATE_REPEAT:
                        state->_ctx._repeat = (struct T_repeat*)malloc(sizeof(struct T_repeat));
                        state->_ctx._repeat->_block = va_arg(argp, struct T_block*);
                        state->_ctx._repeat->_exp = va_arg(argp, struct T_exp*);
                break;

                case STATE_IF:
                        state->_ctx._if = (struct T_if*)malloc(sizeof(struct T_if));
                        state->_ctx._if->_exp = va_arg(argp, struct T_exp*);
                        state->_ctx._if->_block = va_arg(argp, struct T_block*);
                        state->_ctx._if->_fail = va_arg(argp, struct T_block*);
                break;

                case STATE_READ:
                        state->_ctx._read = (struct T_read*)malloc(sizeof(struct T_read));
                        state->_ctx._read->_exp = va_arg(argp, struct T_exp*)->_ctx._variable;
                break;

                case STATE_WRITE:
                        state->_ctx._write = (struct T_write*)malloc(sizeof(struct T_write));
                        state->_ctx._write->_exp = va_arg(argp, struct T_exp*);
                break;

                case STATE_ASSIGN:
                        state->_ctx._assign = (struct T_assign*)malloc(sizeof(struct T_assign));
                        state->_ctx._assign->_lval = va_arg(argp, struct T_exp*)->_ctx._variable;
                        state->_ctx._assign->_rval = va_arg(argp, struct T_exp*);
                break;
        }

        va_end(argp);
        return state;
}

struct T_block* makeBlock(struct T_state* state, struct T_block* block)
{
        struct T_block* root = (struct T_block*)malloc(sizeof(struct T_block));

        root->_state = state;
        root->_next = block;

        return root;
}

extern size_type __lang_err_count;

size_type nerr()
{
        return __lang_err_count;
}

bool_type assert_t(bool_type check)
{
        if (!check) { yyerror("invalid type"); __lang_err_count++; }
        return check;
}

bool_type assert_tm(bool_type check, string_type pmsg)
{
        if (!check) { yyerror(pmsg); __lang_err_count++; }
        return check;
}

bool_type assert_var(bool_type check, struct T_variable* var)
{
        if (!check) { yyerrors("unknown symbol", var->_name, var->_lineno); __lang_err_count++; }
        return check;
}

bool_type isBinary(struct T_exp* exp)
{
        switch (exp->_type)
        {
                case EXP_VARIABLE:
                case EXP_CONSTANT:
                case EXP_SYMMETRIC:
                case EXP_BNEG:
                case EXP_NOT:
                        return 0;

                default: return 1;
                /*
                case EXP_MOD:
                case EXP_DIV:
                case EXP_MULT:
                case EXP_SUBTRACT:
                case EXP_ADD:
                case EXP_BAND:
                case EXP_BXOR:
                case EXP_BOR:
                case EXP_LT:
                case EXP_LE:
                case EXP_GT:
                case EXP_GE:
                case EXP_AND:
                case EXP_OR:
                case EXP_EQ:
                case EXP_DIFF:*/
        }
}

bool_type isConst(struct T_exp* exp) { return exp->_type == EXP_CONSTANT; }
bool_type isVar(struct T_exp* exp)   { return exp->_type == EXP_VARIABLE; }



